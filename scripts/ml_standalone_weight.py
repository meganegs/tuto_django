
# -------------------------------------------------------

print(f'step 1 - imports')

# Import all dependencies
import joblib
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import constants
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression


print() # add return into logging
# ------------Fonctions-------------------------------------------



# Load dataset

def Load_dataset(path):
    print(f'step 2 - Load Dataset')
    #Method pd / ml/data/weight-height.csv
    df = pd.read_csv(path)
    print(df.head())
    
    print(f'--- --- ---')

    return df


print() # add return into logging
# -------------------------------------------------------


def transform():
    print(f'step 3 - Transform US properties into EU ')
    df = Load_dataset("ml/data/weight-height.csv")

# Transform US properties into EU 
    df['Weight'] = np.around(df['Weight'] * constants.pound, 1)
    df['Height'] = np.around(df['Height'] * constants.inch * 100)
    df['Height'] = df['Height'].astype(np.int64, errors='ignore') 
    print(df.head())

    print(f'--- --- ---')
    return df

#transform()


print() # add return into logging
# -------------------------------------------------------


def Balanced():
    print(f'step 4 - Check if Genders is Balanced')
    df = transform()
    genders = df['Gender'].value_counts()
    print(genders)
    print(f'--- --- ---')
    return df


print() # add return into logging
# -------------------------------------------------------
def logging():
    print(f'step 5 - Plot Genders depending on Weight and Height')
    df = Balanced()
    sns.scatterplot(x='Height', y='Weight', data=df, hue='Gender')
# import matplotlib.pyplot as plt
# plt.show()
    print(f'--- --- ---')
    return df 

#logging()

print() # add return into logging
# -------------------------------------------------------

def Replace():
    print(f'step 6 - Replace string gender Male by Int:0 & Female by Int:1')
    df = logging()
    df.Gender = df.Gender.map({"Male" : 0, "Female" : 1})
    print(df.sample(n=10))
    print(f'--- --- ---')

    return df
#Replace()

print() # add return into logging
# -------------------------------------------------------

def Learning_Patterns():
    print(f'step 7 - Train model with Machine Learning Patterns')
    df = Replace()
    X = df[ ["Gender", "Height"] ] # definir input sur l'axe X 
    y = df[ ["Weight"] ] # definir output sur l'axe Y
    
    # Trouver des correlations entre X et Y afin de definir des modeles
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    # Entrainer le dataset avec un modele de regression lineaire.
    lin_reg = LinearRegression()
    lin_reg.fit(X_train, y_train)
    print(lin_reg.score(X_test, y_test))
    print(f'--- --- ---')
    return df

Learning_Patterns()


print() # add return into logging
# -------------------------------------------------------

"""
def model():
    print(f'step 8 - After the model is trained we can query')
    df = Learning_Patterns()

    # Donne moi le poids moyen d'un male de 1.80M
    test = np.round(lin_reg.predict([[0, 180]])[0][0],1)

    # Print le poids retourne
    print(test)
    print(f'--- --- ---')
    return test
print(model())

print() # add return into logging
# -------------------------------------------------------

def save():
print(f'step 9 - Save the trained modele that can be used later')

# Save the model
joblib_file = "WeightPredictionLinRegModel.joblib"
joblib.dump(lin_reg, joblib_file) 

print(f'--- --- ---')

print() # add return into logging
# -------------------------------------------------------
"""